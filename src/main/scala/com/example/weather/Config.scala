package com.example.weather

import com.typesafe.config.ConfigFactory
import io.circe.Decoder
import io.circe.HCursor
import squants.thermal.Temperature

final case class Config(
    units: Units,
    apiKey: String,
    tempMapping: TempMapping) {

  val tempDecoder: Decoder[Temperature] = Decoder.decodeDouble.map(units.toTemperature)

  val subjectiveTempDecoder: Decoder[SubjectiveTemp] = tempDecoder.map(tempMapping.from)
}

object Config {
  //Just load from a default location.  Lots of other strategies are possible,
  //this is just a simple one to make this toy code configurable.
  def load(): Either[Throwable, Config] = {
    val tsConfig = ConfigFactory.parseFile(new java.io.File("./application.conf"))

    import io.circe.config.syntax._
    import io.circe.generic.auto._

    tsConfig.as[Config]
  }

  implicit val configDecoder: Decoder[Config] = new Decoder[Config] {
    override def apply(c: HCursor): Decoder.Result[Config] = {
      val w = c.downField("weather")

      val unitsAndKeyResult = for {
        units <- w.downField("units").as[Units]
        apiKey <- w.downField("apiKey").as[String]
      } yield (units, apiKey)

      unitsAndKeyResult.flatMap { case (units, apiKey) =>
        implicit val tempMappingDecoder = TempMapping.decoder(units)

        for {
          tempMapping <- w.downField("tempMapping").as[TempMapping]
        } yield Config(units, apiKey, tempMapping)
      }
    }
  }
}