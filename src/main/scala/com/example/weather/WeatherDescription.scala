package com.example.weather

import io.circe.Decoder

final case class WeatherDescription(name: String, description: String)

object WeatherDescription {
  implicit val weatherDescriptionDecoder: Decoder[WeatherDescription] = {
    Decoder.forProduct2("main", "description")(WeatherDescription(_, _))
  }
}