package com.example.weather

import squants.thermal.Temperature
import io.circe.Decoder

trait TempMapping {
  def from(t: Temperature): SubjectiveTemp
}

object TempMapping { 
  val default: TempMapping = FromBoundaries(units = Units.Imperial, maxCold = 32, minHot = 70)

  final case class FromBoundaries private (
      units: Units, 
      maxCold: Temperature, 
      minHot: Temperature) extends TempMapping {

    override def from(t: Temperature): SubjectiveTemp = {
      if(t <= maxCold) { SubjectiveTemp.Cold(t) }
      else if(t > maxCold && t < minHot) { SubjectiveTemp.Moderate(t) } 
      else { SubjectiveTemp.Hot(t) }
    }
  }

  object FromBoundaries {
    def apply(units: Units, maxCold: Double, minHot: Double): FromBoundaries = {
      FromBoundaries(units, maxCold = units.toTemperature(maxCold), minHot = units.toTemperature(minHot))
    }
  }

  def decoder(units: Units): Decoder[TempMapping] = {
    Decoder.forProduct2[TempMapping, Double, Double](
      "maxCold", 
      "minHot")(FromBoundaries(units, _, _))
  }
}
