package com.example.weather

import cats.effect.Sync
import io.circe.{ Decoder}
import org.http4s._
import org.http4s.circe._

import io.circe.HCursor


final case class Weather(
  description: List[WeatherDescription], 
  subjectiveTemp: SubjectiveTemp,
  alerts: List[Alert],
  units: Units
) {
  def humanReadable: String = {
    val degreesPart = units match {
      case Units.Imperial => subjectiveTemp.temp.inFahrenheit
      case Units.Metric => subjectiveTemp.temp.inCelsius
    }

    val subjectiveTempPart = s"${subjectiveTemp.name} (${degreesPart})"

    s"""
    |Basic: ${description.map(_.name).mkString(",")}
    |Description: ${description.map(_.description).mkString(",")}
    |${subjectiveTempPart}
    |Alerts: ${if(alerts.isEmpty) "None" else alerts.map(_.humanReadable).mkString(System.lineSeparator)}
    """.stripMargin.trim
  }
}

object Weather {
  implicit def weatherDecoder(implicit config: Config): Decoder[Weather] = new Decoder[Weather] {
    final def apply(c: HCursor): Decoder.Result[Weather] = {
      val current = c.downField("current")

      implicit val tempDecoder: Decoder[SubjectiveTemp] = config.subjectiveTempDecoder

      for {
        desc <- current.downField("weather").as[List[WeatherDescription]]
        subjectiveTemp <- current.downField("temp").as[SubjectiveTemp]
        alerts <- c.downField("alerts").as[Option[List[Alert]]]
      } yield {
        Weather(desc, subjectiveTemp, alerts = alerts.getOrElse(Nil), config.units)
      }
    }
  }

  implicit def weatherEntityDecoder[F[_]: Sync](implicit config: Config): EntityDecoder[F, Weather] = jsonOf
}