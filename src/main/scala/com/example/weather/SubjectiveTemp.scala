package com.example.weather

import squants.thermal.Temperature

sealed abstract class SubjectiveTemp(val name: String, val temp: Temperature)

object SubjectiveTemp {
  final case class Hot(override val temp: Temperature) extends SubjectiveTemp("Hot", temp)
  final case class Cold(override val temp: Temperature) extends SubjectiveTemp("Cold", temp)
  final case class Moderate(override val temp: Temperature) extends SubjectiveTemp("Moderate", temp)
}