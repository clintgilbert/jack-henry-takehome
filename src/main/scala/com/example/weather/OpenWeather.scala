package com.example.weather

import org.http4s.Uri


object OpenWeather {
  def requestUri(
      units: Units,
      lat: Double,
      lon: Double,
      apiKey: String): Uri = {

    //TODO: a better, safer way
    Uri.unsafeFromString(s"http://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=${units.name}&appid=${apiKey}")
  }
}