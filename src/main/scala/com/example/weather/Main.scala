package com.example.weather

import cats.effect.{ExitCode, IO, IOApp}
import scala.concurrent.ExecutionContext.global
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.blaze.BlazeServerBuilder
import cats.effect.ConcurrentEffect
import cats.effect.Timer
//import org.http4s.server.middleware.Logger
import org.http4s.implicits._

object Main extends IOApp {
  def run(args: List[String]) = {
    WeatherServer.stream[IO].toTry.get.compile.drain.as(ExitCode.Success)
  }

  object WeatherServer {
    def stream[F[_]: ConcurrentEffect](implicit T: Timer[F]): Either[Throwable, fs2.Stream[F, Nothing]] = {
      Config.load().map { implicit config =>
        (for {
          client <- BlazeClientBuilder[F](global).stream
          service = WeatherService.impl[F](client)
          httpApp = WeatherRoutes.weatherRoutes[F](service, config).orNotFound
          finalHttpApp = httpApp//Logger.httpApp(true, true)(httpApp)
          exitCode <- BlazeServerBuilder[F](global)
            .bindHttp(8080, "0.0.0.0")
            .withHttpApp(finalHttpApp)
            .serve
        } yield exitCode).drain
      }
    }
  }
}
