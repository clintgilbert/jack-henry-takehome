package com.example.weather

import cats.implicits._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.QueryParamDecoderMatcher
import cats.effect.ConcurrentEffect

object WeatherRoutes {

  private object latQueryParam extends QueryParamDecoderMatcher[Double]("lat")
  private object lonQueryParam extends QueryParamDecoderMatcher[Double]("lon")
  
  def weatherRoutes[F[_]: ConcurrentEffect](weatherService: WeatherService[F], config: Config): HttpRoutes[F] = { 

    val dsl = new Http4sDsl[F]{}

    import dsl._

    HttpRoutes.of[F] {
      case GET -> Root / "weather" :? latQueryParam(lat) +& lonQueryParam(lon) =>
        for {
          weather <- weatherService.get(lat, lon, config.units)
          resp <- Ok(weather.humanReadable)
        } yield resp
    }
  }
}