package com.example.weather

import squants.thermal.Temperature
import squants.thermal.Celsius
import squants.thermal.Fahrenheit
import io.circe.Decoder
import org.http4s.ParseFailure


sealed abstract class Units(val name: String) {
  def unapply(s: String): Boolean = s == name

  def toTemperature(t: Double): Temperature

  def toDouble(t: Temperature): Double = this match {
    case Units.Metric => t.toCelsiusScale
    case Units.Imperial => t.toFahrenheitScale
  }
}

object Units {
  case object Metric extends Units("metric") {
    override def toTemperature(t: Double): Temperature = Celsius(t)
  }
  case object Imperial extends Units("imperial") {
    override def toTemperature(t: Double): Temperature = Fahrenheit(t)
  }

  def fromString(s: String): Either[String, Units] = s.trim.toLowerCase match {
    case Metric() => Right(Metric)
    case Imperial() => Right(Imperial)
    case x => Left(s"Unknown unit type: '${x}'")
  }

  def parseString(s: String): Either[ParseFailure, Units] = {
    fromString(s).left.map(msg => ParseFailure(msg, msg))
  }

  implicit val unitsDecoder: Decoder[Units] = Decoder.decodeString.emap(fromString)
}
