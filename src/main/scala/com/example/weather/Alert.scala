package com.example.weather

import java.time.LocalDateTime
import io.circe.Decoder
import io.circe.HCursor
import scala.util.Try
import java.time.ZoneOffset

final case class Alert(
    senderName: String,
    eventType: String,
    start: LocalDateTime,
    end: LocalDateTime,
    description: String) {

  def humanReadable: String = s"""
  |Type: $eventType
  |From: $senderName
  |Starting: $start
  |Ending:   $end
  |Details: $description
  |""".stripMargin.trim
}

object Alert {
  implicit val alertDecoder: Decoder[Alert] = new Decoder[Alert] {
    final override def apply(c: HCursor): Decoder.Result[Alert] = {
      val timestampDecoder: Decoder[LocalDateTime] = {
        def parseUtcUnixTimestamp(ts: Long): LocalDateTime = LocalDateTime.ofEpochSecond(ts, 0, ZoneOffset.UTC)

        Decoder.decodeLong.emapTry(ts => Try(parseUtcUnixTimestamp(ts)))
      }

      for {
        senderName <- c.downField("sender_name").as[String]
        eventType <- c.downField("event").as[String] 
        //There must be a better way to compose Decoders :\
        start <- timestampDecoder.tryDecode(c.downField("start"))
        end <- timestampDecoder.tryDecode(c.downField("end"))
        description <- c.downField("description").as[String]
      } yield {
        Alert(
          senderName = senderName,
          eventType = eventType,
          start = start,
          end = end,
          description = description)
      }
    }
  }

  
}