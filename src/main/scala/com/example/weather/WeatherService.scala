package com.example.weather

import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.Method._
import cats.effect.Sync
import cats.implicits._

trait WeatherService[F[_]]{
  def get(lat: Double, lon: Double, units: Units): F[Weather]
}

object WeatherService {
  final case class WeatherError(e: Throwable) extends RuntimeException

  def impl[F[_]: Sync](client: Client[F])(implicit config: Config): WeatherService[F] = new WeatherService[F] {
    private val dsl = new Http4sClientDsl[F]{}

    import dsl._

    def get(lat: Double, lon: Double, units: Units): F[Weather] = {
      val uri = OpenWeather.requestUri(units = units, lat = lat, lon = lon, apiKey = config.apiKey)

      client.expect[Weather](GET(uri)).adaptError { case e => WeatherError(e) }
    }
  }
}