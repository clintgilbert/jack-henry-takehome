package com.example.weather

import munit.FunSuite

final class OpenWeatherTest extends FunSuite {
  test("requestUri - metric") {
    import OpenWeather.requestUri

    val actual = requestUri(Units.Metric, lat = 12.34, lon = 34.56, apiKey = "foo")

    assert(actual.toString == "http://api.openweathermap.org/data/2.5/onecall?lat=12.34&lon=34.56&units=metric&appid=foo")
  }

  test("requestUri - imperial") {
    import OpenWeather.requestUri

    val actual = requestUri(Units.Imperial, lat = 12.34, lon = 34.56, apiKey = "foo")

    assert(actual.toString == "http://api.openweathermap.org/data/2.5/onecall?lat=12.34&lon=34.56&units=imperial&appid=foo")
  }
}
