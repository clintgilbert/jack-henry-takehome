package com.example.weather

import munit.FunSuite
import squants.thermal.Celsius
import squants.thermal.Fahrenheit

final class UnitsTest extends FunSuite {
  import Units.{Metric, Imperial}

  test("name") {
    assert(Metric.name == "metric")
    assert(Imperial.name == "imperial")
  }

  test("toTemperature") {
    assert(Metric.toTemperature(-10) == Celsius(-10))
    assert(Metric.toTemperature(0) == Celsius(0))
    assert(Metric.toTemperature(42) == Celsius(42))

    assert(Imperial.toTemperature(-10) == Fahrenheit(-10))
    assert(Imperial.toTemperature(0) == Fahrenheit(0))
    assert(Imperial.toTemperature(42) == Fahrenheit(42))
  }

  test("fromString") {
    assert(Units.fromString("metric") == Right(Metric))
    assert(Units.fromString("Metric") == Right(Metric))
    assert(Units.fromString("METRIC") == Right(Metric))

    assert(Units.fromString("imperial") == Right(Imperial))
    assert(Units.fromString("Imperial") == Right(Imperial))
    assert(Units.fromString("IMPERIAL") == Right(Imperial))

    assert(Units.fromString("").isLeft)
    assert(Units.fromString("asdasdasd").isLeft)
  }

  test("parseString") {
    assert(Units.parseString("metric") == Right(Metric))
    assert(Units.parseString("Metric") == Right(Metric))
    assert(Units.parseString("METRIC") == Right(Metric))

    assert(Units.parseString("imperial") == Right(Imperial))
    assert(Units.parseString("Imperial") == Right(Imperial))
    assert(Units.parseString("IMPERIAL") == Right(Imperial))

    assert(Units.parseString("").isLeft)
    assert(Units.parseString("asdasdasd").isLeft)
  }
}
