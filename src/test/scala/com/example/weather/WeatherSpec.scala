package com.example.weather

import cats.effect.IO
import org.http4s._
import org.http4s.implicits._
import munit.CatsEffectSuite
import org.http4s.client.blaze.BlazeClientBuilder

class WeatherSpec extends CatsEffectSuite {

  test("Valid request returns status code 200") {
    assertIO(retWeather.map(_.status) ,Status.Ok)
  }

  private[this] val retWeather: IO[Response[IO]] = {
    import scala.concurrent.ExecutionContext.global

    BlazeClientBuilder[IO](global).resource.use { client =>
      implicit val config = Config.load().toTry.get

      val weather = WeatherService.impl[IO](client)

      val getHW = Request[IO](Method.GET, uri"/weather?lat=42.8509&lon=72.5579")

      WeatherRoutes.weatherRoutes(weather, config).orNotFound(getHW)
    }
  }
}